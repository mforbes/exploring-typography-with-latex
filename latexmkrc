# latexmk Init File; -*-perl-*-

# xelatex
$pdf_mode = 5;
$dvi_mode = 0;
$postscript_mode = 0;

$out_dir = '_build';
mkdir $out_dir;
