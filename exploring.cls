% Document class for exploring page design.

\ProvidesClass{exploring}[2012/01/26]
\providecommand{\exclude}[1]{}

\newif\iftitlesec\titlesecfalse
\DeclareOption{titlesec}{\titlesectrue}
\ProcessOptions\relax

\LoadClass{scrbook}
%\LoadClass[usemarginnote=false]{mmf}
\RequirePackage{etoolbox}
\RequirePackage{xparse}

\RequirePackage{geometry}
\geometry{verbose=true}

\RequirePackage{marginnote}
\newkomafont{marginnote}{\footnotesize}
\newkomafont{chapternumber}{}
\renewcommand*{\marginfont}{\usekomafont{marginnote}}

\iftitlesec
  % titlesec is incompatible with KOMA script in general.
  % We at least need to define all the styles.  From the docs, these are the standard
  % cls defaults
  \RequirePackage{titlesec}
  \titleformat{\chapter}[display]
    {\normalfont\huge\bfseries}{\chaptertitlename\ \thechapter}{20pt}{\Huge}
  \titleformat{\section}
    {\normalfont\Large\bfseries}{\thesection}{1em}{}
  \titleformat{\subsection}
    {\normalfont\large\bfseries}{\thesubsection}{1em}{}
  \titleformat{\subsubsection}
    {\normalfont\normalsize\bfseries}{\thesubsubsection}{1em}{}
  \titleformat{\paragraph}[runin]
    {\normalfont\normalsize\bfseries}{\theparagraph}{1em}{}
  \titleformat{\subparagraph}[runin]
    {\normalfont\normalsize\bfseries}{\thesubparagraph}{1em}{}
  \titlespacing*{\chapter}      {0pt}{50pt}{40pt}
  \titlespacing*{\section}      {0pt}{3.5ex plus 1ex minus .2ex}{2.3ex plus .2ex}
  \titlespacing*{\subsection}   {0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}  
  \titlespacing*{\subsubsection}{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}
  \titlespacing*{\paragraph}    {0pt}{3.25ex plus 1ex minus .2ex}{1em}
  \titlespacing*{\subparagraph} {\parindent}{3.25ex plus 1ex minus .2ex}{1em}
\fi

\RequirePackage{textcase}
\RequirePackage{calc}
\RequirePackage[debug]{debugging}
\RequirePackage{array}
\RequirePackage{setspace}

% Markup
\providecommand{\pkg}[1]{\texttt{#1}}
\providecommand{\cls}[1]{\texttt{#1}}
\providecommand{\cmd}[1]{\texttt{\textbackslash#1}}

% Defining commands: use pgfkeys
% http://tex.stackexchange.com/questions/34312/
% http://tex.stackexchange.com/questions/26771/
%
\RequirePackage{pgfkeys}
\def\ex@oldargs{}
\pgfkeys{/ex/.is family, /ex,
  default/.style = {
    pagewidth=8.5in, 
    pageheight=11in,
    border=1in,
    bindingoffset=1in,
    baselineskip=12pt,
  },
  border/.code = \edef\ex@border{#1}
    \edef\ex@oldargs{\ex@oldargs,border=\noexpand{#1}},
  pagewidth/.code = \edef\ex@pagewidth{#1}
    \edef\ex@oldargs{\ex@oldargs,pagewidth=\noexpand{#1}},
  pageheight/.code = \edef\ex@pageheight{#1}
    \edef\ex@oldargs{\ex@oldargs,pageheight=\noexpand{#1}},
  bindingoffset/.code = \edef\ex@bindingoffset{#1}
    \edef\ex@oldargs{\ex@oldargs,bindingoffset=\noexpand{#1}},
  baselineskip/.code = \edef\ex@@baselineskip{#1}
    \edef\ex@oldargs{\ex@oldargs,baselineskip=\noexpand{#1}},
  .unknown/.code = \edef\ex@args{\ex@args,\pgfkeyscurrentname=\noexpand{#1}}
    \edef\ex@oldargs{\ex@oldargs,\pgfkeyscurrentname=\noexpand{#1}},
}

% My geometry
\newcommand\mygeometry[1]{%
  \gdef\ex@oldargs{}
  \updatemygeometry{#1}
}
\newcommand\updatemygeometry[1]{%
  \def\ex@args{}
  \edef\all@args{{/ex, default, \ex@oldargs, #1}}
  \expandafter\pgfkeys\all@args
  \edef\ex@pwidth{\dimexpr\ex@pagewidth + \numexpr 2*\ex@border
                  + \ex@bindingoffset\relax}
  \edef\ex@pheight{\dimexpr\ex@pageheight + \numexpr 2*\ex@border\relax}
  \xdef\ex@args{
    {
      papersize={\ex@pwidth,\ex@pheight},
      layoutsize={\dimexpr\ex@pagewidth + \ex@bindingoffset\relax,
        \ex@pageheight},
      layoutoffset={\ex@border,\ex@border},
      includemp=false,          % Margins are computed for text block.
      includehead=false,
      includefoot=false,
      bindingoffset=\ex@bindingoffset,
      heightrounded=true,
      \ex@args
    }
  }
  \ex@baselineskip=\ex@@baselineskip
  \baselineskip=\ex@baselineskip
  \topskip=\ex@baselineskip
  %\pdfpagewidth=\ex@pwidth
  %\pdfpageheight=\ex@pheight
  %\paperwidth=\ex@pwidth
  %\paperheight=\ex@pheight
  \Gm@begindocumenthook
  \expandafter\newgeometry\ex@args
  \Gm@begindocumenthook
  %\savegeometry{geometryA}
  %\loadgeometry{geometryA}
}

% Grid typesetting
\newlength\ex@fontsize
\newlength\ex@baselineskip      % Set by \mygeometry

\newcommand\ex@setfontsize[1]{%
  \global\ex@fontsize=#1%
  \renewcommand\normalsize{%
    \baselineskip=\ex@baselineskip%
    \@setfontsize\normalsize{\ex@fontsize}{\ex@baselineskip}%
    \lineskip=0pt
    \lineskiplimit=-\ex@fontsize%
    \abovedisplayskip=\baselineskip%
    \abovedisplayshortskip=.5\baselineskip%
    \belowdisplayskip=\abovedisplayskip
    \belowdisplayshortskip=\abovedisplayshortskip
    \parskip=0pt % No glue between paragraphs...
    \let\@listi\@listI}}

\newcommand\setfontsize[1]{%
  \KOMAoptions{fontsize=#1}
  \ex@setfontsize{#1}
  \normalsize
}

% This rule will take exactly \baselinskip space, maintaining the grid. The
% raise value is the height above the next baseline. It will extend down
% thickness.
% \trule[thickness][raise]
\NewDocumentCommand\trule{O{0.4pt}O{0pt}}{
  \vskip0pt\vtop to0pt{
    \noindent\raisebox{#2}{\vbox{\leavevmode\hrule height#1}}}
}

\AtBeginDocument{
}
% Reset marginpar spacing
\AfterEndPreamble{
  %\let\@addmarginpar\ubc@original@@addmarginpar
  %\let\marginnote\ubc@original@marginnote
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Clear to open on left page
% http://tex.stackexchange.com/a/11709
\newcommand*{\cleartoleftpage}{%
  \clearpage
    \if@twoside
    \ifodd\c@page
      \hbox{}\newpage
      \if@twocolumn
        \hbox{}\newpage
      \fi
    \fi
  \fi
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Drawing stuff on each page.  Here we modify the "current page" node in tikz
% so that it properly represents the page geometry.  See:
% http://www.texample.net/tikz/examples/modifying-current-page-node/

\RequirePackage{tikz}
\RequirePackage{tkz-euclide}

\usetikzlibrary{decorations.pathmorphing}

% Helper function to define new nodes
\newcommand\ex@newnode[5]{%
  \expandafter\def\csname pgf@sh@ns@#1\endcsname{rectangle}
  \Ifthispageodd{%
    \expandafter\def\csname pgf@sh@np@#1\endcsname
    {\def\southwest{\pgfpoint#2}\def\northeast{\pgfpoint#3}}%
  }{%
    \expandafter\def\csname pgf@sh@np@#1\endcsname
    {\def\southwest{\pgfpoint#4}\def\northeast{\pgfpoint#5}}}
  \expandafter\def\csname pgf@sh@nt@#1\endcsname{{1}{0}{0}{1}{0pt}{0pt}}
  \expandafter\def\csname pgf@sh@pi@#1\endcsname{pgfpageorigin}}

\ex@newnode{current paper}                               % Name
           { {0pt}{0pt} }{ {\paperwidth}{\paperheight} } % Odd pages
           { {0pt}{0pt} }{ {\paperwidth}{\paperheight} } % Even pages

\newcommand\setpagenode{
  \ex@newnode{current page}
  % Odd pages (right)
  { {\Gm@layouthoffset+\Gm@bindingoffset}
    {\paperheight-\Gm@layoutheight-\Gm@layoutvoffset} }
  { {\Gm@layouthoffset+\Gm@layoutwidth}{\paperheight-\Gm@layoutvoffset} }
  % Even pages (left)
  { {\Gm@layouthoffset}{\paperheight-\Gm@layoutheight-\Gm@layoutvoffset} }
  { {\Gm@layouthoffset+\Gm@layoutwidth-\Gm@bindingoffset}
    {\paperheight-\Gm@layoutvoffset} }
  %
  \ex@newnode{current text}
  % Odd pages (right)
  { {\Gm@layouthoffset+\Gm@bindingoffset+\Gm@lmargin}
    {\paperheight-\textheight-\Gm@tmargin-\Gm@layoutvoffset} }
  { {\Gm@layouthoffset+\Gm@bindingoffset+\Gm@lmargin+\textwidth}
    {\paperheight-\Gm@tmargin-\Gm@layoutvoffset} }
  % Even pages (left)  
  { {\Gm@layouthoffset+\Gm@rmargin}
    {\paperheight-\textheight-\Gm@tmargin-\Gm@layoutvoffset} }
  { {\Gm@layouthoffset+\Gm@rmargin+\textwidth}
    {\paperheight-\Gm@tmargin-\Gm@layoutvoffset} }

  \ex@newnode{current binding}
  % Odd pages (right)
  { {\Gm@layouthoffset}{0pt} }
  { {\Gm@layouthoffset+\Gm@bindingoffset}{\paperheight} }
  % Even pages (left)  
  { {\Gm@layouthoffset+\Gm@layoutwidth-\Gm@bindingoffset}{0pt} }
  { {\Gm@layouthoffset+\Gm@layoutwidth}{\paperheight} }
}

% Shows the various boundaries on the current page.
\newcommand\showpagegeometry{
\begin{tikzpicture}[remember picture, overlay]
  \draw[line width=10pt,yellow,very nearly transparent]
       (current paper.north west) rectangle (current paper.south east);
  \fill[blue,very nearly transparent]
       (current page.north west) rectangle (current page.south east);
  \fill[red,very nearly transparent]
       (current binding.north west) rectangle (current binding.south east);
  \fill[green,very nearly transparent]
       (current text.north west) rectangle (current text.south east);
\end{tikzpicture}}
\newcommand{\showdps}{
  \cleartoleftpage
  \showpagegeometry
  \clearpage
  \showpagegeometry
}

\newcommand\showpagegrid{
\begin{tikzpicture}[remember picture, overlay]
  \node at (current text.north west) {
    \begin{tikzpicture}[remember picture, overlay]
      \draw[help lines] (0, -\textheight) grid [ystep=\baselineskip]
      (\textwidth, 0);
    \end{tikzpicture}
  };
\end{tikzpicture}}



% Force recalculation of the current page node whenever the overlay option is
% used. If you page is centered on your stock paper it is only necessary to
% calculate the current page rectangle once.
\pgfkeys{/tikz/overlay/.add code={\setpagenode}{\setpagenode}}
  


\define@key{paperkeys}{x}{\def\mypaperx{#1}}
\define@key{paperkeys}{y}{\def\mypapery{#1}}
\tikzdeclarecoordinatesystem{paper}
{%
  \setkeys{paperkeys}{#1}%
  \pgfpointadd{\pgfpointanchor{(current page.south west)}{center}}
              {\pgfpointxy{\mypaperx}{\mypapery}}%
}






% Convert from one set of units to the other and display things
% http://tex.stackexchange.com/a/8337/6903
\def\ex@convertto#1#2{\strip@pt\dimexpr #2*65536/\number\dimexpr #1}
\def\ex@setunit#1#2{
  \def\ex@unit{#1}
  \def\ex@unitlabel{#2}
}
\ex@setunit{1pt}{pt}
\def\ex@the#1{\ex@convertto{\ex@unit}{#1}\ex@unitlabel}

% Shows the various vertical glue
\RequirePackage{multicol}
\newcommand\showvglue{
  \def\exshow##1{\noindent\textbf{\string##1\relax:} \the##1 = \ex@the##1\par}
  {\sffamily
    %\begin{multicols}{2}
      \exshow{\baselineskip}
      \exshow{\topskip}
      \exshow{\parskip}
    %\end{multicols}
  }
}








\providecommand{\exclude}[1]{}
\exclude{

%\DeclareOption{mmfthm}{\PassOptionsToPackage{mmfthm}{mmfmath}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{ubcthesis_new}}
\ProcessOptions\relax

\LoadClass[usemarginnote,
           %screen=1.6,          % Aspect ratio of half my display at home
           %screen=1.25,          % Aspect ratio of half my laptop display
           thesis=false]
          {ubcthesis_new}
\RequirePackage{styles/ctmmf}
\RequirePackage{styles/ctmmf_heads}
\RequirePackage[dottedtoc=false,noparttocnum,noparttocpage]{styles/ctmmf_toc_titletoc}

\RequirePackage{etoolbox}
\RequirePackage{etextools}
\RequirePackage{mciteplus}

\appto{\ubc@beforegeometryhook}{
  % We do this outside of the preamble so that the configuration will *not* be
  % pre-compiled, allowing changes to be detected by latexmk, thereby
  % triggering a recompilation.
  \InputIfFileExists{localconfig.sty}{
    \message{mmf.cls: Reading local configuration from 'localconfig.sty'}
  }{
    \message{mmf.cls: No local configuration found 'localconfig.sty'}
  }
}

\let\monthname\@undefined\relax
\RequirePackage[icon=Note,opacity=0]{pdfcomment}

\RequirePackage{graphicx}
\RequirePackage[numbers,sort&compress]{natbib}
\RequirePackage{mmfmath}

\RequirePackage{siunitx}[2011/06/15 2.2i]
\sisetup{detect-all=true}       % Not working?

\RequirePackage{MnSymbol}

\RequirePackage[toc,acronym,style=tree]{glossaries}
\renewcommand*{\acronymfont}[1]{\upsc{#1}}
\RequirePackage{multicol}

% Use a two-column format.  We want to base this on the "list" style rather than
s% the "tree" style because the latter shows the "symbol" (which looks weird!)
\newglossarystyle{twocol}{%
  \glossarystyle{list}%
  \BeforeBeginEnvironment{theglossary}{
    \begin{multicols}{2}
      \setkomafont{descriptionlabel}{\bfseries\selectfont}
      \setlist*[description]{noitemsep,leftmargin=0pt}
    }
  \AfterEndEnvironment{theglossary}{
    \end{multicols}
  }
}
\glossarystyle{twocol}

% Use this to define new acronyms.
\newcommand\na[3][\@empty]{%
  {%
    \ifx#1\@empty
      \def\short{\upsc{#2}}
    \else
      \def\short{#1}
    \fi
    \expandnext{\newacronym{#2}}{\short}{#3}
  }
}

\RequirePackage{listings}
\definecolor{background}{rgb}{1.0,1.0,1.0}
\definecolor{rule}{rgb}{1.0,0.0,0.0}

% This puts a red line flush with the left margin
\lstset{
  language=python,
  morekeywords={assert},
  columns=flexible,
  basicstyle=\fontfamily{fve}\small, % print whole listing small
  frame=leftline,
  framexleftmargin=0pt,
  framesep=4pt,
  xleftmargin=5pt,
  % This is a little hack to replace the python prompt with a nicer symbol
  % that we typeset in the margin.  Not sure why -1 is required...
  literate=*{>>>}{\llap{\color{gray}\scriptsize$\ggg$\hspace{8pt}}}{-1}
            {...}{\llap{\color{gray}$\cdots$\hspace{9pt}}}{-1},
  rulecolor=\color{rule}}
% This keeps the code flush with the left margin.  The rule extends into the
% margin.
\lstset{
  framexleftmargin=0pt,
  framesep=4pt,
  xleftmargin=0pt}

\lstdefinelanguage{maple}{
  morekeywords={D, I, NULL, Pi, 
    additionally, algsubs, and, arccos, arcsin, arctan, assume, assuming,
    asympt,
    break, by,
    catch, collect, combine, convert, cos,
    diff, do,
    elif, else, end, error, evalc evalm, exp, expand,  export,
    false, fi, for, from,
    global,
    if, in, int, infinity,
    latex, ln, local, local,
    module,
    nargs, not, 
    od, option, or, 
    polynom, proc, 
    restart, return, 
    series, simplify, sin, size, solve, sqrt, subs, symbolic,
    tan, then, to, true, try,
    use,
    while,
    xnor, xor},
  sensitive=true,
  morecomment=[l]{\#},
  morecomment=[s]{(*}{*)},
  morestring=[b]",
  literate=*{>\ }{\llap{\color{gray}\scriptsize>\hspace{8pt}} }{-1}
  %columns=fixed,     % Output requires alignment, but if you only use
  %basewidth=0.5em,   % latex() then you don't need this.
}
\newcommand*{\code}{%
  \lstinline[breaklines=true,breakatwhitespace=true,
  basicstyle=\fontfamily{fve}\fontshape{sl}\selectfont]}

\newcommand*{\pweaveinline}{\lstinline[identifierstyle=,keywordstyle=]}

\lstnewenvironment{pweavelisting}[4][]{%
  \lstset{language=#2,label=chunk:#4,%
          aboveskip=-0.5\baselineskip,
          belowskip=0.5\baselineskip,#1}%
  \noindent
  \newline
  \raisebox{-0.5\baselineskip+4pt}[0pt][0pt]{\makebox[\textwidth]{
    \hfill\lstinline[identifierstyle=,keywordstyle=,%
                     basicstyle=\tiny\selectfont]|#3|}}%
}{\noindent\ignorespacesafterend}

% Use kpfonts for sans serif because they have smallcaps.
\usepackage[nomath,notext]{kpfonts} %
\renewcommand\sf{%
  \fontfamily{jkpss}\fontsize{11.2}{\f@baselineskip}\selectfont}
\renewcommand\textsf[1]{{\sf #1}}
\setkomafont{note}{\sf}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Uppercase Small Caps
% See
% http://tex.stackexchange.com/questions/39831/uppercase-smallcaps-with-latex-not-xelatex
% This solution is only for acronyms (no spaces, paragraphs etc. allowed)

\DeclareRobustCommand*\fullsc[1]{\scalebox{1.06}[1.09]{\textsc{#1}}}

\long\def\mmf@appendto#1#2{#1=\expandafter{\the#1#2}}
\long\def\mmf@concatto#1#2{%
  \expandafter\mmf@concataux\expandafter{\the#2}#1{\the#1}}
\long\def\mmf@concataux#1#2#3{#2=\expandafter{#3#1}}

\newtoks\mmf@sc@toks
\newtoks\mmf@toks
\let\mmfsc\fullsc
\newif\ifsc@active
\def\sc@end{\ifsc@active\mmfsc{\the\mmf@sc@toks}\sc@activefalse\fi}
\protected\def\upsc#1{%
  \mmf@toks={}%
  \sc@activefalse\mmf@upsc#1\@nil\sc@end}
\def\mmf@upsc#1{\ifx#1\@nil\else\mmf@@upsc{#1}\expandafter\mmf@upsc\fi}
\def\mmf@@upsc#1{%
  \ifnum\catcode`#1=11\relax % Letter    
    \lowercase{\mmf@appendto\mmf@toks{#1}}%
    \ifnum\uccode`#1=`#1\relax%      
      \ifsc@active\else\sc@activetrue\mmf@sc@toks={}\fi%
      \mmf@concatto{\mmf@sc@toks}{\mmf@toks}%
    \else%
      \sc@end%
      \the\mmf@toks%
    \fi%
    \mmf@toks={}%
  \else%
    \mmf@appendto\mmf@toks{#1}%
  \fi%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Useful new commands

% Use this for equations in the margin.  It allows them to be flush right or
% left depending on the page.  See http://tex.stackexchange.com/questions/30515/equations-in-the-margin-automatic-flush-right-or-left
\newenvironment{marginarray}
{\arraycolsep=1.4pt\par$\array{rl}}
{\endarray$\\}

\newcommand{\todo}[2][]{\pdfcomment[#1]{#2}}
\newcommand{\file}[1]{\href{file:#1}{\texttt{#1}}}
} %\exclude
