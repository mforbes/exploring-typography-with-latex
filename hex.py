r"""Python code to calculate Bringhurst's hexagonal layout.

We use the sympy geometry package to construct the layout and then solve for the
unknown parameters.

A couple of notes:
* The geometry module is quite broken so you need to be careful.  In particular,
  intersection points that exist may not be found.
* It is important to use Rational numbers in the calculations for this reason.
"""
from __future__ import division
import numpy as np
import scipy.optimize
from matplotlib import pyplot as plt

import sympy
from sympy import Rational, cos, sin, var
from sympy import pi as Pi
from sympy.geometry import Point, Ray, Line, intersection

def draw(l, close=True):
    x = [_p.x for _p in l]
    y = [_p.y for _p in l]
    if close and x:
        x.append(x[0])
        y.append(y[0])
    plt.plot(x, y, 'o-')
    plt.gca().set_aspect('equal')


# Define the anchoring hexagon
hex = [Point(0,0)]
for theta in xrange(5):
    hex.append(hex[-1] + Point(cos(theta*Pi/3), sin(theta*Pi/3)))

page = [None, None, None, None]
l = [None,]*10
_a, _b = var(['a','b'])
_p = Point(_a, _b)

def go_xy(_x, _y):
    r"""Given fractions of x and y along their edges, draw the rest."""
    x = Point((1-_x)*hex[0].x + _x*hex[1].x, (1-_x)*hex[0].y + _x*hex[1].y)
    y = Point((1-_y)*hex[4].x + _y*hex[3].x, (1-_y)*hex[4].y + _y*hex[3].y)
    page[0] = intersection(Line(hex[5], x), Line(hex[0], y))[0]
    page[2] = intersection(Line(hex[5], hex[3]), Line(hex[2], y))[0]
    page[3] = Point(page[0].x, page[2].y)
    page[1] = Point(page[2].x, page[0].y)
    a = intersection(Ray(hex[4], page[3]), Line(hex[1], hex[2]))[0]
    j = intersection(Line(x, hex[2]), Line(hex[0], a))[0]
    l[0] = hex[0]
    l[1] = hex[4]
    l[2] = a
    l[3] = hex[0]
    l[4] = y
    l[5] = hex[2]
    l[6] = x
    l[7] = hex[5]
    l[8] = hex[3]
    l[9] = hex[1]
    err_j = j - page[1]
    return Point(err_j.x, err_j.y)

err = go_xy(_a, _b)
eq = sympy.Eq(err.x, err.y)
    
def f(x):
    return [err.x.subs(dict(a=x[0], b=x[1])).n(),
            err.y.subs(dict(a=x[0], b=x[1])).n()]

res = scipy.optimize.fsolve(f, [0.4,  0.8],
                            xtol=1e-13)

def eval(x):
    plt.clf()
    go_xy(Rational(str(x[0])), Rational(str(x[1])))
    for _n, _p in enumerate(page):
        page[_n] = _p.evalf()
    for _n, _l in enumerate(l):
        l[_n] = _l.evalf()
    map(draw, [hex,page])
    draw(l, close=False)

eval(res)


# Are the lines parallel? Nope!
np.allclose(0, np.diff(map(float, 
                           [Line(l[1], l[2]).slope.evalf(), 
                            Line(l[7], l[6]).slope.evalf(),
                            Line(l[4], l[5]).slope.evalf()])))


print(page)
print("a=%s, x=%s, y=%s" % (l[2], l[6], l[4]))

lines = 42
if False:
    # Determine page size from baselineskip = 12pt
    baselineskip = 12/72.27
    units = baselineskip*lines/(page[2] - page[0]).y.evalf()
else:
    # Determine 
    paperwidth = 5.2 # Inches
    units = paperwidth/(hex[1].x-hex[0].x)
    baselineskip = text[1]/lines*72.27

unit = 1.0 # Base unit in inches
def toa(p):
    return np.array([sympy.N(p.x), sympy.N(p.y)])*units/unit

page = toa(hex[3] - hex[0])
text = toa(page[2] - page[0])
inner, bottom = toa(page[0]-hex[0])
outer, top = toa(hex[3]-page[2])
bcorr = 0.2

string = r"""
\newlength\unit
\setlength\unit{{{unit}in}}
\baselineskip={baselineskip}pt
\newgeometry{{
  layoutsize={{{page[0]}\unit,{page[1]}\unit}},
  layoutoffset={{0.7in, 0.5in}},
  showframe=true,
  includemp=false,              % Margins are computed for text block.
  includehead=false,
  includefoot=false,
  bindingoffset={bcorr}\unit,
  inner={i}\unit,
  outer={o}\unit,
  top={t}\unit,
  bottom={b}\unit,
  footskip=3\baselineskip, % approx
  marginparsep=0.2in,
  marginparwidth=0.66in, % approx
  twoside=true,
}}
""".format(unit=unit, page=page, bcorr=bcorr,
           i=inner, o=outer, t=top, b=bottom,
           baselineskip=baselineskip)
print string
