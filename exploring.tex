\documentclass[titlesec]{exploring}

\usepackage{geometry}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,shapes.geometric}

\usepackage{lipsum}
\usepackage{fontspec}

\begin{document}

\tableofcontents

\part{Introduction}
\chapter{Introduction}
This document demonstrates different approaches to page layout.  After this
introduction that describes some of our strategies, we simulate several
different layouts.

\section{Document Class}
We use the KOMA \cls{scrbook} class as it provides many nice features, including
a common interface for fonts.

\section{Paper Size}
We use the \pkg{geometry} package to define the page size.  The
\cmd{newgeometry} command allows us to redefine almost everything on the fly
except for the actual paper size.

\section{Fonts}
As mentioned above, we use the KOMA font selection system, especially the
\cmd{setkomafont} command (and the \cmd{usekomafont} internally).  We define the
following
\begin{description}
\item[marginnote:] For margin notes.
\item[chapternumber:] For the chapter number (often big and in the margin).
\end{description}

\section{Chapter Heads}
The design of section and chapter heads is done using the \pkg{titlesec} package
which I find is very flexible.

\section{Grid Typography}
We follow some of the tips presented in the \pkg{grid} package to implement
grid-based typography.  In particular, you should use the command
\cmd{setfontsize} to set the main font.  This will establish the grid.

\part{Robert Bringhurst's ``Elements of Typographic Style''}
\chapter{About the Style}
Bringhurst's page layout is based on the hexagon as described pictorally on page
6 of his book.  The paper size is $5.2" \times 9"$ (listed on page 148)
excluding binding adjustments etc.  (Amazon lists the dimensions as $5.4" \times
9.2"$ -- presumably this includes an extra $0.2"$ for the cover extension.)  The
inner and outer margins are approximately $0.61"$ and $1.04"$ respectively,
while the top and bottom margins are approximately $0.59"$ and $2.0"$.  He has
42 lines per page, so given the page-height of $7.32" = 12.6$~pt.

\newcommand\BringhurstHex{
    \path (0,0) coordinate[label=below:{ll}] (ll)
    -- (1,0) coordinate[label=below:{lr}] (lr)
    -- ++(60:1) coordinate[label=right:{r}] (r)
    -- ++(120:1) coordinate[label=above:{ur}] (ur)
    -- ++(180:1) coordinate[label=above:{ul}] (ul)
    -- ++(240:1) coordinate[label=left:{l}] (l)
    -- ++(300:1) coordinate (ll);
    \coordinate[label=below right:{a}] 
                  (a) at (1.27106916798686, 0.469505571318668);
    \coordinate (tll) at (0.117300021398114, 0.294835256924744);
    \coordinate (tlr) at (0.798192881206518, 0.294835256924744);
    \coordinate (tur) at (0.798192881206518, 1.61553741320908);
    \coordinate (tul) at (0.117300021398114, 1.61553741320908);
    \coordinate[label=below:{x}] (x) at (0.435936138301, 0);
    \coordinate[label=above:{y}] (y)   at (0.689095323638, 1.73205080756888);
    \tkzInit[ymin=0,ymax=2,xmin=-0.5,xmax=2]
    \tkzInterLL(l,ur)(a,ul) \tkzGetPoint{A}
    \tkzInterLL(ll,y)(ul,a) \tkzGetPoint{B}
    \tkzInterLL(l,ur)(ll,y) \tkzGetPoint{C} 
    \tkzInterLL(ll,a)(l,x) \tkzGetPoint{D}
    \tkzDefCircle[in](A,B,C) \tkzGetPoint{U} \tkzGetLength{rU}
    \tkzDefCircle[in](tll,tlr,D) \tkzGetPoint{L} \tkzGetLength{rL}

    % Locate page number
    \tkzDefShiftPoint[L](1,0){Lr}
    \tkzInterLL(L,Lr)(tur,tlr) \tkzGetPoint{pagenumber}

    % Locate running header
    \tkzDefShiftPoint[U](0,\rU pt){Uu}
    \tkzDefShiftPoint[Uu](1,0){Uur}
    \tkzInterLL(Uu,Uur)(y,r) \tkzGetPoint{header}
    
    \color{black};
    \draw (ll) -- (lr) -- (r) -- (ur) -- (ul) -- (l) -- cycle;
    \color{gray};
    \draw (ur) -- (l) -- (x) -- (r) -- (y) -- (ll) -- (a) -- (ul);
    \draw (l) -- (ur);
    \draw (tul) -- (tur) -- (tlr) -- (tll) -- cycle;
    \draw (ul) -- (ur) -- (lr) -- (ll) -- cycle;
    
    \tkzSetUpLine[color=gray]
    \tkzDrawCircle[R](U, \rU pt)
    \tkzDrawCircle[R](L, \rL pt)
    \tkzDrawSegments(Uu,header L,pagenumber)
}
\begin{figure}[h]
  \begin{tikzpicture}[scale=5.2in/1cm/2]
    \BringhurstHex
  \end{tikzpicture} 
\end{figure}

\clearpage
% Set the textblock and margin note space

\newcommand\lines{42}
\newlength\unit
\setlength\unit{1.0in} % Tweaked to get justification working...
\setlength\baselineskip{\unit*\real{6.86765}/\lines}
\mygeometry{% Comment needed
  baselineskip=\baselineskip,
  %baselineskip=12.0pt,
  showcrop=true,
  showframe=true,
  pagewidth=5.2\unit,
  pageheight=9.00666419935816\unit,
  border=1in,
  border=0in,
  bindingoffset=1in,
  bindingoffset=0in,
  showframe=true,
  inner=0.60996011127019\unit,
  outer=1.04939701772611\unit,
  top=0.60586965067094\unit,
  %bottom=1.53314333600935\unit,
  lines=\lines,
  footskip=3\baselineskip, % approx
  %footskip=2.37814 pt,
  footskip=31.410473 pt,        % 2.37814*2.54*5.2 where 2.37814 = \rL
  marginparsep=\baselineskip,
  marginparwidth=0.75\unit, % approx
  twoside=true,
}

\setfontsize{10pt}
\makeatletter
\the\baselineskip, \the\textheight, \the\ex@baselineskip
\makeatother

\showvglue
\the\baselineskip
\the\textheight
\the\topskip
\the\pdfpagewidth
\the\pdfpageheight
\Gmlogcontent


% Since we change the text width, we need to tell marginnote.
\xdef\marginnotetextwidth{\the\textwidth}

% Set the font
\defaultfontfeatures{Scale=MatchLowercase,
                     Mapping=tex-text}
\setmainfont[Mapping=tex-text, % E.g. -- -> en-dash
             Numbers=OldStyle,
             UprightFeatures={LetterSpace=-0.9},
             ItalicFeatures={LetterSpace=0.9},    % To cancel -0.9 tracking
             SmallCapsFeatures={LetterSpace=10.0},
             ]{Minion Pro}

\setkomafont{marginnote}{
  \addfontfeature{UprightFeatures={LetterSpace=5}}%
  \addfontfeature{ItalicFeatures={LetterSpace=5}}%
  \fontsize{7pt}{9pt}\selectfont}
\setkomafont{chapternumber}{%
  \fontspec[Numbers=OldStyle]{TeX Gyre Pagella}
  \fontsize{72pt}{72pt}\selectfont}
\setkomafont{sectioning}{\normalfont}
\setkomafont{chapter}{\addfontfeature{UprightFeatures={LetterSpace=15.0}}}

\setkomafont{section}{\scshape}
\setkomafont{subsection}{\itshape}

% Chapters open on the right page.
\KOMAoptions{open=right}

\setlength{\parindent}{1.5em}
% Chapter formatting
\iftitlesec
  \titleformat{name=\chapter}
              [block]
              {} % format
              {\marginnote{\usekomafont{chapternumber}\thechapter}
                          [3\baselineskip]} % label
              {0pt} % separation between label and heading
              {\raggedright\usekomafont{chapter}\MakeTextUppercase} % before
              [\trule] % after
  \titlespacing*{\chapter}{0pt}{-\topskip}{0pt}

  % Bringuest has the whole text flush left, so do not typeset the label
  % independently here or the text will hang.
  \titleformat{name=\section}
              {\usekomafont{section}} % format
              {} % label
              {0pt} % separation between label and heading
              {\thesection\hspace{0.5em}\MakeTextLowercase} % before
  \titleformat{name=\subsection}
              {\usekomafont{subsection}} % format
              {} % label
              {0pt} % separation between label and heading
              {\raggedright{\normalfont\thesubsection}\hspace{0.5em}} % before

  \titlespacing*{\section}{0pt}{0pt}{\baselineskip} 
  \titlespacing*{\subsection}{0pt}{\baselineskip}{\baselineskip} 
\else
\fi
\showdps
\makeatletter
\begin{tikzpicture}[remember picture, overlay]
  \node at (current page.south west) {
    \begin{tikzpicture}[remember picture, overlay, 
      scale=(\Gm@layoutwidth-\Gm@bindingoffset)/1cm]
      \BringhurstHex
    \end{tikzpicture}
  };
\end{tikzpicture}
\makeatother
% Demo chapter 3.
\cleardoublepage
\setcounter{chapter}{2}

\chapter{Harmony \& Counterpoint}\showpagegrid

\section{Size}

\subsection{Don't compose without a scale.}

The simplest scale is a single note, and sticking with a single note draws more
attention to other parameters, such as rhythm and inflection. The early
Renaissance typographers set each book in a single font -- that is, one face in
one size -- supplemented by hand-drawn or specially engraved large initial
letters for the openings of chapters. Their pages show what sensuous evenness of
texture and variety of rhythm can be attained with a single font of type: very
much greater than on a typewriter, where letters have, more often than not, a
single width and a single stroke-weight as well as a single size.

In the sixteenth century, a series of common sizes developed among European
typographers, and the series survived with 
\marginnote{A few examples of the many older names for type sizes:
  \tabcolsep=0pt
  \begin{tabular}{@{}r@{\hspace{0.1em}}>{\raggedright}p{0.45in}}
    6 pt: & \emph{nonpareil}\tabularnewline
    7 pt: & \emph{minion}\tabularnewline
    8 pt: & \emph{brevier} or \emph{small text}\tabularnewline
    9 pt: & \emph{bourgeois} or \emph{galliard}\tabularnewline
    10 pt: & \emph{long primer} or \emph{garamond}\tabularnewline
    11 pt: & \emph{small pica} or \emph{philosophy}\tabularnewline
    12 pt: & \emph{pica}\tabularnewline
    14 pt: & \emph{english} or \emph{augustin}\tabularnewline
    18 pt: & \emph{great primer}
  \end{tabular}
}[0.5ex]
lit\-tle change and few additions for
400 years. In the early days, the sizes had names rather than numbers, but
measured in points, the traditional series is this:

% Need to center the lower text
\def\do#1{\vbox{\hbox{\fontsize{#1pt}{#1pt}\selectfont a}
                \hbox{\fbox{\makebox[\width]{\scriptsize\textsf{#1}}}}}\hfill}
\def\do#1{\shortstack{%
    {\raisebox{-14\baselineskip}{\fontsize{#1pt}{#1pt}\selectfont a}}\\
    {\scriptsize\textsf{#1}}}\hfill}

%%%% Hack to preserve grid...
\vspace{6\baselineskip}
\noindent\raisebox{2\baselineskip}[0pt][0pt]{%
  \makebox[\textwidth]{
  \rule{0pt}{6\baselineskip}
  \docsvlist{6,7,8,9,10,11,12,14,16,18,21,24,36,48,60}\do{72}}
}

This is the typographic equivalent of the diatonic scale. But modern equipment
makes it possible to set, in addition to these sizes, all the sharps and flats
and microtonal intervals between.  Twenty-point, 22-point, 23-point, and
10�-point type are all available for the asking. The designer can now choose a
new scale or tone-row for every piece of work.

These new resources are useful, but rarely all at once. Use the old familiar
scale, or use new scales of your own devising, but limit yourself, at first, to
a modest set of distinct and related intervals.  Start with one size and work
slowly from there. In time, the scales you choose, like the faces you choose,
will become recognizable features of personal style.
\lipsum[1-5]
\showpagegrid
\lipsum[6-10]
\chapter{Structural Forms \& Devices}
\section{Openings}
\subsection{Make the title page a symbol of the dignity and presence of
the text.}
If the text has immense reserve and dignity, the title page should have these
properties as well -- and if the text is devoid of dignity, the title page
should in honesty be the same.

Think of the blank page as alpine meadow, or as the purity of undifferentiated
being. The typographer enters this space and must change it. The reader will
enter it later, to see what the typographer has done. The underlying truth of
the blank page must be infringed, but it must never altogether disappear -- and
whatever displaces it might well aim to be as lively and peaceful as it is. It
is not enough, when building a title page, merely to unload some big,
prefabricated letters into the center of the space, nor to dig a few holes in
the silence with typographic heavy machinery and then move on. Big type, even
huge type, can be beautiful and useful. But poise is usually far more important
than size -- and poise consists primarily of emptiness. Typographically, poise
is made of white space. Many fine title pages consist of a modest line or two
near the top, and a line or two near the bottom, with little or nothing more
than taut, balanced white space in between.

\subsection{Don't permit the titles to oppress the text.}
In books, spaced capitals of the text size and weight are often perfectly
adequate for titles. At the other extreme, there is a fine magazine design by
Bradbury Thompson, \marginnote{For examples of Thompson's work, see Bradbury
  Thompson, \textit{The Art of Graphic Design} (1988)} in which the title, the
single word \textsc{boom}, is set in gigantic bold condensed caps that fill the
entire two-page spread. The text is set in a tall narrow column \emph{inside the
  stem} of the big B. The title has swallowed the text -- yet the text has been
reborn, alive and talkative, like Jonah from the whale.

Most unsuccessful attempts at titling fall between these two
extremes, and their problem is often that the title throws its weight
around, unbalancing and discoloring the page. If the title is set in
a larger size than the text, it is often best to set it u\&{}lc in a light

\part{Explorations in Typography}
\chapter{About the Style}
This part explores some of the layouts considered in the book Explorations in
Typography.  The paper size is 9.25" by 12" (probably 9.5" with 0.2" binding and
0.05" trimmed).  With the exception of rotated layouts, each layout is on an
8.5" by 11" insert with a 0.25" offset at the top and a 0.5" offset from the
outside (trimmed slightly in the version I have).  Within this layout, each page
is styled independently, however, information about the layout is added around
the periphery.

For this design, we set the paper-size to match, and use the layout as the 8.5"
by 11" demo, tweaking as needed.

\setlength\unit{1.0in} % Tweaked to get justification working...

\mygeometry{% Comment required
  baselineskip=12pt,
  showcrop=true,
  showframe=true,
  pagewidth=9.5\unit,
  pageheight=12\unit,
  layoutsize={8.5\unit, 11\unit},
  layoutoffset={0.5\unit, 0.25\unit},
  border=0in,
  bindingoffset=0.0in,
  showframe=true,
  inner=1.2\unit,
  top=0.75\unit,
  bottom=1\unit,
  textheight=7.75\unit,       % Takes us right to bottom for foot-positioning
  footskip=0.6\unit,
  marginparsep=\baselineskip,
  marginparwidth=0.75\unit, % approx
  twoside=true,
}
\setfontsize{10pt}
\showvglue
\Gmlogcontent

% Since we change the text width, we need to tell marginnote.
\xdef\marginnotetextwidth{\the\textwidth}

% Set the font
\defaultfontfeatures{Scale=MatchLowercase,
                     Mapping=tex-text}
\setmainfont[Mapping=tex-text, % E.g. -- -> en-dash
             Numbers=OldStyle,
             UprightFeatures={LetterSpace=-0.9},
             ItalicFeatures={LetterSpace=0.9},    % To cancel -0.9 tracking
             SmallCapsFeatures={LetterSpace=10.0},
             ]{Minion Pro}

\newcommand\showExplorationLayout{
  \begin{tikzpicture}[remember picture, overlay]
    \color{gray};
    \draw (current page.south west) rectangle (current page.north east);
  \end{tikzpicture}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chapter style
\setkomafont{chapter}{\color{orange}\fontspec{Helvetica}\sffamily\bfseries}
\newcommand\chaptergraphics{
  \begin{tikzpicture}[remember picture, overlay]
    \fill[color=gray,very nearly transparent]
    (current page.south west) rectangle (current page.north east);
  \end{tikzpicture}
}
\iftitlesec
  \titleformat{name=\chapter}
              [block]
              {\cleardoublepage\pagestyle{empty}} % format
              {} % label
              {0pt} % separation between label and heading
              {\usekomafont{chapter}Exploration \thechapter\ / } % before
              [\chaptergraphics\clearpage] % after
  \titlespacing*{\chapter}{0pt}{0pt}{0pt}
\else
\fi


\updatemygeometry{% Comment required
  baselineskip=12pt,
  inner=1.2\unit,
  top=0.75\unit,
  bottom=1\unit,
  textheight=7.75\unit,       % Takes us right to bottom for foot-positioning
  footskip=0.6\unit,
  marginparsep=\baselineskip,
  marginparwidth=0.75\unit, % approx
  twoside=true,
}
\chapter{Paragraph Indicator: Indent}
\setmainfont{Courier}
\fontsize{10pt}{12pt}
\selectfont


\columnsep=2pc
\begin{multicols}{4}
\noindent
The first exploration uses a two-column layout with simple indentation for each
paragraph.
\end{multicols}

\setmainfont[Mapping=tex-text, % E.g. -- -> en-dash
             Numbers=OldStyle,
             UprightFeatures={LetterSpace=-0.9},
             ItalicFeatures={LetterSpace=0.9},    % To cancel -0.9 tracking
             SmallCapsFeatures={LetterSpace=10.0},
             ]{Helvetica}
\fontsize{10pt}{12pt}
\selectfont

\clearpage
\begin{multicols}{2}
\raggedright
\setlength{\parindent}{1em}   % Must come after \raggedright!
\noindent Ever since people have been writing things down, they have had to
consider their audience before actually putting pen to paper: letters would have
to look different depending on whether they were to be read by mainly other
people (in official documents or inscriptions), just one other person (in a
letter), or only the writer (in a notebook or diary). There would be less room
for guesswork if letter shapes were made more formal as the diversity of the
readership expanded.\showExplorationLayout

Some of the first messages to be read by a large number of people were
rendered not by pens but by chisels. Large inscriptions on monuments in ancient
Rome were carefully planned, with letter drawn on the stone with a brush before
they were chiseled. Even if white-out had existed in those days, it would not
have helped to remove mistakes made in stone. A bit of planning was also more
important then, since stonemasons were sometimes more expendable than slabs of
marble.

Graphic design and typography are complicated activities, but even the simple
projects benefit from thinking about the problem, forming a mental picture of
the solution, and then carefully planning the steps between.

Scientists have not been content with just calling the human face ``beautiful''
if it meets certain ideals, or ``ugly'' if it doesn't. They had to go out and
measure proportions of nose to jaw, forehead to chin, and so on to establish why
some faces are more appealing than others.

Typographers and graphic designers often choose typefaces for the very same
reason they might fancy a person: They just like that person. For more
scientifically-minded people, however, there are specific measurements,
components, details, and proportions to describe various parts of a
letter. While these won't tell you what makes a typeface good, they will at
least give you the right words to use when you discuss the benefits of a
particular face over another. You can say ``I hate the x-height on
Such-a-Gothic'' or ``These descenders just don't work for me'' or ``Please, may
I see something with a smaller cap height?'' and you'll know what you are
talking about.

\columnbreak

While metal letters could be made to any width and height, digital type has to
conform to multiples of the smallest unit: the pixel. Every character has to be
a certain number of pixels wide and high. This is not a problem when the letters
are made up of 600 pixels per inch, as is the case with modern laser
printers--those pixels are not discernible to our eyes, and we are happy to
believe that we are looking at smooth curves instead of little squares fitted
into tight grids.

On screens, however, only 72 pixels make up one inch. We could see each and
every one of them if engineers hadn't already found ways around that. Computer
screens, however are not where we read all of our type these days--phones,
smartphones, even microwave ovens all have displays. Most screen displays are
small and simple, which means black on greenish gray. And the type unmistakably
consists of bitmaps: this means that an 8-point letter is actually made up of
eight pixels. If we allow six pixels above the baseline, including accents, and
two below for descenders, that leaves only three or four pixels for a lowercase
character. Despite these restrictions, there are hundreds of bitmap fonts, each
unique by a matter of a few pixels, but enough to prove that typographic variety
cannot be suppressed by technological constraints.

Rhythm and contrast keep coming up when discussing good music and good
typographic design. They are concepts that also apply to spoken language, as
anyone who has had to sit through a monotonous lecture will attest; the same
tone, volume and speed of speech will put even the most interested listener into
dreamland. Every now and again the audience needs to be shaken, either by a
change in voice or pitch, by a question being posed, or by the speaker talking
very quietly and then suddenly shouting. An occasional joke also works, just as
the use of a funny typeface can liven up a page.
\end{multicols}
\end{document}
