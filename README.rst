.. -*- rst -*- -*- restructuredtext -*-

.. This file should be written using the restructure text
.. conventions.  It will be displayed on the bitbucket source page and
.. serves as the documentation of the directory.

=================================
 Exploring Typography with LaTeX
=================================
This project was inspired by the book `Explorations in Typography`_.  I use a
modified form of the geometry_ package to allow one to change the page size mid
-document, thereby allowing one to explore different layouts and typographic
choices in a single document (this may require pdfLaTeX).

Examples of layout design currently include:

`The Elements of Typographic Style`_ : Robert Bringhurst
   Examples of the chapter and layout matching in particular the first page of
   chapter 3.  Also, the geometric form of the page layout is demonstrated and
   computed with a small python script.  (I have not yet found a way of
   computing/constructing the layout using TikZ_ or metapost_.  Does such a
   construction exist?)

   This uses the Minion Pro font, but presently requires XeLaTeX.  (In principle
   one can generate the metrics and use this with LaTeX but this requires a
   non-trivial amount of work.)

`Explorations in Typography`_ : Carolina de Bartolo with Erik Spiekermann
  Example from chapter 6.

Here is the result of typesetting the document with XeLaTeX using `latexmk` (the
result will be placed in the `_build` directory):

`PDF Example`_
==============

This project is just in the exploratory stage with several hacks.  Eventually I
hope to roll the page-changing parts back into the geometry_ package, and
separate the rest.  Please feel free to make suggestions, add layouts, or fix
the code (just fork the project on bitbucket, play with it, and make a Pull
Request if you have something I should add to the repository.)

.. _Explorations in Typography: http://explorationsintypography.com/
.. _The Elements of Typographic Style: \
   http://en.wikipedia.org/wiki/The_Elements_of_Typographic_Style
.. _geometry: http://www.ctan.org/tex-archive/macros/latex/contrib/geometry
.. _TikZ: http://www.texample.net/tikz/
.. _metapost: http://www.tug.org/metapost.html
.. _PDF Example: \
   https://bitbucket.org/mforbes/exploring-typography-with-latex/downloads/exploring.pdf
